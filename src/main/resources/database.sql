
DROP TABLE IF EXISTS project;
DROP TABLE IF EXISTS student;

CREATE TABLE student (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    mail VARCHAR(128) NOT NULL,
    session VARCHAR(24)
);
CREATE TABLE project (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    techs VARCHAR(128),
    picture VARCHAR(256),
    id_student INT,
    FOREIGN KEY (id_student) REFERENCES student(id) ON DELETE CASCADE
);

INSERT INTO student (name, mail, session) VALUES 
('Habibi', 'hab.simplon@mail.com', 'promo 1'),
('Arthur', 'art.hur@mail.com', 'promo 1'),
('Mei', 'mei.simplon@mail.com', 'promo 2'),
('Mariko', 'mariko@mail.com', 'promo 2');


INSERT INTO project (name, techs, picture, id_student) VALUES 
('Projet Blog', 'Spring, Angular', 'https://cdn.pixabay.com/photo/2014/02/13/07/28/wordpress-265132_960_720.jpg', 1),
('Projet Budget', 'Angular', 'https://cdn.pixabay.com/photo/2017/05/07/13/27/wallet-2292428_960_720.jpg', 1),
('Projet Jeu', 'Java', 'https://cdn.pixabay.com/photo/2019/05/26/19/46/tic-tac-toe-4231109_960_720.jpg', 1),
('Projet Budget', 'Angular', 'https://cdn.pixabay.com/photo/2017/05/07/13/27/wallet-2292428_960_720.jpg', 2),
('Projet Jeu', 'Java', 'https://cdn.pixabay.com/photo/2019/05/26/19/46/tic-tac-toe-4231109_960_720.jpg', 2),
('Projet Blog', 'Spring, Angular', 'https://cdn.pixabay.com/photo/2014/02/13/07/28/wordpress-265132_960_720.jpg', 3),
('Projet Budget', 'Angular', 'https://cdn.pixabay.com/photo/2017/05/07/13/27/wallet-2292428_960_720.jpg', 3),
('Projet Jeu', 'Java', 'https://cdn.pixabay.com/photo/2019/05/26/19/46/tic-tac-toe-4231109_960_720.jpg', 3);