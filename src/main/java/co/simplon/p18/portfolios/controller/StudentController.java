package co.simplon.p18.portfolios.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.portfolios.entity.Student;
import co.simplon.p18.portfolios.repository.ProjectRepository;
import co.simplon.p18.portfolios.repository.StudentRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping("/api/student")
public class StudentController {

    @Autowired
    private StudentRepository stuRepo;
    @Autowired
    private ProjectRepository proRepo;

    @GetMapping
    public List<Student> getAll() {
        return stuRepo.findAll();
    }

    @GetMapping("/{id}")
    public Student getOne(@PathVariable int id) {
        Student student = stuRepo.findById(id);
        if(student == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        student.setProjects(proRepo.findByIdStudent(id));
        return student;
    }
    

    
    
    
}
