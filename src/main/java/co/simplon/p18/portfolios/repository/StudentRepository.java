package co.simplon.p18.portfolios.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.portfolios.entity.Student;

@Repository
public class StudentRepository {
    @Autowired
    private DataSource dataSource;

    public List<Student> findAll() {
        List<Student> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM student");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToStudent(rs));
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

        return list;
    }

    public List<Student> findBySession(String session) {
        List<Student> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM student WHERE session=?");

            stmt.setString(1, session);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToStudent(rs));
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

        return list;
    }

    public Student findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM student WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            
            if (rs.next()) {
              return sqlToStudent(rs);
                
            }
            
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }
        return null;
    }

    public void save(Student student) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO student (name, mail, session) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, student.getName());
            stmt.setString(2, student.getMail());
            stmt.setString(3, student.getSession());

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                student.setId(rs.getInt(1));
            }

        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM student WHERE id=?");
            stmt.setInt(1, id);

            return (stmt.executeUpdate() == 1);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

    public boolean update(Student student) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE student SET name = ?, mail = ?, session = ? WHERE id = ?");

            stmt.setString(1, student.getName());
            stmt.setString(2, student.getMail());
            stmt.setString(3, student.getSession());
            stmt.setInt(4, student.getId());

            return (stmt.executeUpdate() == 1);

        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

    }

    protected static Student sqlToStudent(ResultSet rs) throws SQLException {
        return new Student(
            rs.getInt("id"),
            rs.getString("name"),
            rs.getString("mail"),
            rs.getString("session"));
    }

}
