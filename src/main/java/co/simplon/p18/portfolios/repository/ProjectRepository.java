package co.simplon.p18.portfolios.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.portfolios.entity.Project;

@Repository
public class ProjectRepository {
    @Autowired
    private DataSource dataSource;

    public List<Project> findAll() {
        List<Project> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM project");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToProject(rs));
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

        return list;
    }

    public List<Project> findByIdStudent(int idStudent) {
        List<Project> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM project WHERE id_student=?");

            stmt.setInt(1, idStudent);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(sqlToProject(rs));
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

        return list;
    }

    public Project findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM project WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return sqlToProject(rs);
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

        return null;
    }

    public void save(Project project) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO project (name, techs, picture, id_student) VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, project.getName());
            stmt.setString(2, project.getTechs());
            stmt.setString(3, project.getPicture());
            stmt.setInt(4, project.getStudent().getId());

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                project.setId(rs.getInt(1));
            }

        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM project WHERE id=?");
            stmt.setInt(1, id);

            return (stmt.executeUpdate() == 1);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error",e);
        }
    }

    public boolean update(Project project) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE project SET name = ?, techs = ?, picture = ? WHERE id = ?");

            stmt.setString(1, project.getName());
            stmt.setString(2, project.getTechs());
            stmt.setString(3, project.getPicture());
            stmt.setInt(4, project.getId());

            return (stmt.executeUpdate() == 1);

        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error", e);
        }

    }


    protected static Project sqlToProject(ResultSet rs) throws SQLException {
        return new Project(
            rs.getInt("id"),
            rs.getString("name"),
            rs.getString("techs"),
            rs.getString("picture"));
    }

}
