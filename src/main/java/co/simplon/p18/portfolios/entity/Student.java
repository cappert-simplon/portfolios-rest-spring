package co.simplon.p18.portfolios.entity;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class Student {
    private Integer id;
    @NotBlank
    private String name;
    @Email
    @NotBlank
    private String mail;
    private String session;

    private List<Project> projects = new ArrayList<>();
    
    public Student() {
    }
    public Student(@NotBlank String name, @Email @NotBlank String mail, String session) {
        this.name = name;
        this.mail = mail;
        this.session = session;
    }
    public Student(Integer id, @NotBlank String name, @Email @NotBlank String mail, String session) {
        this.id = id;
        this.name = name;
        this.mail = mail;
        this.session = session;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getMail() {
        return mail;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    public String getSession() {
        return session;
    }
    public void setSession(String session) {
        this.session = session;
    }
    public void addProject(Project project) {
        projects.add(project);
    }
    public List<Project> getProjects() {
        return projects;
    }
    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}
