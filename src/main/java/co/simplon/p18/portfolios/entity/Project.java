package co.simplon.p18.portfolios.entity;

import javax.validation.constraints.NotBlank;

public class Project {
    private Integer id;
    @NotBlank
    private String name;
    private String techs;
    private String picture;
    private Student student;
    public Project() {
    }
    public Student getStudent() {
        return student;
    }
    public void setStudent(Student student) {
        this.student = student;
    }
    public Project(@NotBlank String name, String techs, String picture) {
        this.name = name;
        this.techs = techs;
        this.picture = picture;
    }
    public Project(Integer id, @NotBlank String name, String techs, String picture) {
        this.id = id;
        this.name = name;
        this.techs = techs;
        this.picture = picture;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getTechs() {
        return techs;
    }
    public void setTechs(String techs) {
        this.techs = techs;
    }
    public String getPicture() {
        return picture;
    }
    public void setPicture(String picture) {
        this.picture = picture;
    }
    
}
