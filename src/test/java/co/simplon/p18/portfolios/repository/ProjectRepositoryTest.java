package co.simplon.p18.portfolios.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import co.simplon.p18.portfolios.entity.Project;
import co.simplon.p18.portfolios.entity.Student;

@SpringBootTest
@Sql("/database.sql")
public class ProjectRepositoryTest {
   
    @Autowired
    ProjectRepository repo;

    @Test
    void testDelete() {
        assertThat(repo.delete(1)).isTrue();

    }

    @Test
    void testDeleteNotExists() {
        assertThat(repo.delete(100)).isFalse();

    }

    @Test
    void testFindAll() {
        assertThat(repo.findAll())
                .hasSize(8)
                .doesNotContainNull()
                .allSatisfy(project -> assertThat(project).hasNoNullFieldsOrPropertiesExcept("student"));
    }

    @Test
    void testFindById() {
        assertThat(repo.findById(1))
                .hasNoNullFieldsOrPropertiesExcept("student");
    }
    @Test
    void testFindByIdNotExists() {
        assertThat(repo.findById(100))
                .isNull();
    }

    @Test
    void testFindByIdStudent() {
        assertThat(repo.findByIdStudent(1))
                .hasSize(3)
                .doesNotContainNull()
                .allSatisfy(project -> assertThat(project).hasNoNullFieldsOrPropertiesExcept("student"));

    }

    @Test
    void testSave() {
        Project project = new Project("nom", "java", "test");
        project.setStudent(new Student(1,null, null, null));

        repo.save(project);

        assertThat(project.getId()).isNotNull();
    }

    @Test
    void testUpdate() {
        Project project = new Project(1, "nom", "java", "test");

        assertThat(repo.update(project)).isTrue();
    }

    @Test
    void testUpdateNotExists() {
        Project project = new Project(100, "nom", "mail@mail.com", "promo test");

        assertThat(repo.update(project)).isFalse();
    }
}
